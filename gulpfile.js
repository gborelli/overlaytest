var gulp = require('gulp');
var less = require('gulp-less');
// var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();


gulp.task('build-less', function () {
  return gulp.src('src/overlay.less')
    .pipe(less())
    .pipe(gulp.dest('./dist'))
    .pipe(browserSync.stream());
});


gulp.task('copy-html', function() {
  gulp.src([
    'src/**/*.html'
  ])
  .pipe(gulp.dest('dist'))
  .pipe(browserSync.stream());
});

gulp.task('copy-js', function() {
  gulp.src('src/**/*.js')
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.stream());
});


// Copy all static assets
gulp.task('copy', ['copy-html', 'copy-js']);


var gulpBaseTasks = [
  'build-less',
  'copy'
];

gulp.task('build', gulpBaseTasks);

gulp.task('watch', ['build'], function() {
  browserSync.init({
    server: {
      baseDir: './dist',
      https: true
    }
  });

  gulp.watch('src/**/*.js', ['copy-js']);
  gulp.watch('src/**/*.html', ['copy-html']);
  // gulp.watch('src/**/*.json', ['copy-html']);
  gulp.watch('src/**/*.less', ['build-less']);
});


gulp.task('default', ['build']);
