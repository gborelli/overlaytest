(function (tools) {

  var overlays = [];

  var extend = function (){
    for(var i=1; i<arguments.length; i++) {
      for(var key in arguments[i]) {
        if(arguments[i].hasOwnProperty(key)) {
          if (typeof arguments[0][key] === 'object'
              && typeof arguments[i][key] === 'object') {
            extend(arguments[0][key], arguments[i][key]);
          } else {
            arguments[0][key] = arguments[i][key];
          }
        }
      }
    }
    return arguments[0];
  };

  var Overlay = function(trigger, settings) {
    if (settings) {
      this.settings = extend(this.settings, settings);
    }
    this.trigger = trigger;
    this.overlayId = this.trigger.getAttribute('rel');
    this.overlay = document.getElementById(this.overlayId);

    if (this.overlay) {
      this.innerDiv = this.overlay.querySelector('div');
      this.closeTrigger = this.overlay.querySelector('.' + this.settings.closeButtonClass);
      this.bindActions();
    } else {
      console.debug('no overlay found for ' + this.trigger.getAttrivrel);
    }
  };

  Overlay.prototype = {
    trigger: null,
    closeTrigger: null,
    overlayId: null,
    overlay: null,
    innerDiv: null,
    settings: {
      cssClass: 'overlay',
      closeButtonClass: 'close',
      display: 'flex'
    },

    bindActions: function () {
      var self = this;

      if (self.closeTrigger) {
        self.closeTrigger.addEventListener('click', function (e) {
          e.preventDefault();
          self.close();
        });
      } else {
        console.debug('not close link set.');
      }

    },

    toggle: function () {
      console.debug('TODO');
    },

    open: function () {
      var self = this;
      for (var i=0; i < overlays.length; i+=1) {
        overlays[i].close();
      }

      if (self.overlay) {
        self.overlay.className += ' active';
      }
    },

    close: function () {
      var self = this;
      if (self.overlay) {
        self.overlay.className = self.overlay.className.replace('active', '');
      }
    }

  };

  var OverlayManager = function (settings) {
    if (settings) {
      this.settings = extend(this.settings, settings);
    }
    this.init();
  };

  OverlayManager.prototype = {
    settings: {
      cssClass: 'overlay',
      triggerCssClass: 'overlay-trigger',
      closeButtonClass: 'close'
    },

    init: function () {
      var triggers = document.querySelectorAll('.' + this.settings.triggerCssClass);
      for (var i = 0; i < triggers.length; i += 1) {
        var el = triggers[i],
          overlay = new Overlay(el, this.settings);

        overlays.push(overlay);
        el.overlay = overlay;

        el.addEventListener('click', function (e) {
          var el = e.target;
          e.preventDefault();
          el.overlay.open();
        });
      }

    }
  };

  extend(tools, {
    Overlay: Overlay,
    OverlayManager: OverlayManager
  });

})(window.tools = window.tools || {});
